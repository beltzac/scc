﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace SqlCustomCommands
{
    public class XmlFile
    {
        [XmlIgnore]
        public string FileName { get; set; }

        [XmlIgnore]
        public string FileLocation { get; set; }
    }
}
