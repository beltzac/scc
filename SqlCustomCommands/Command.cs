﻿using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace SqlCustomCommands
{
    public class Command : XmlFile
    {       
        public string Help { get; set; }
        public List<Step> Steps { get; set; }

        private Command()
        {           
        }

        public List<string> UsedConnections()
        {
            List<string> used = new List<string>();

            foreach (Step p in Steps)
                used.AddRange(p.Connections);

            return used.Select(s => s.ToLower()).Distinct().ToList();
        }

        public void RunCommand(List<string> parameters, Connections connections)
        {
            Program.WriteVerbose($"Executando {FileName}...");

            parameters = parameters?.Skip(1).ToList(); //Sem o nome do comando

            foreach (Step item in Steps)
            {
                foreach (string connectionName in item.Connections)
                {
                    NpgsqlConnection Con = connections.AllConnections.First(c => c.ConnectionFile.Name.ToLower() == connectionName.ToLower()).Con;

                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = Con;
                        cmd.CommandText = item.Sql;

                        NpgsqlCommandBuilder.DeriveParameters(cmd);

                        int count = 0;
                        foreach (NpgsqlParameter p in cmd.Parameters.ToArray())
                        {
                            p.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Unknown;
                            p.Value = parameters[count];
                            count++;
                        }

                        using (DataTable dt = new DataTable())
                        {
                            dt.Load(cmd.ExecuteReader());
                            if (dt.Rows.Count > 0)
                                Output(dt, item.Format, item.Hash);
                        }
                    }
                }
            }
        }

        public void Output(DataTable data, string format, string hash)
        {

            if (string.IsNullOrEmpty(format) || format.ToLower() == "csv")
            {
                Console.WriteLine(data.ToCsv());
                return;
            }

            if (format.ToLower() == "html")
            {
                Console.WriteLine(data.ToHtmlTable(hash).FormatHTML());
                return;
            }

            if (format.ToLower() == "html-searchable")
            {
                Console.WriteLine("<html>");
                Console.WriteLine("<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>");
                Console.WriteLine("<body>");
                Console.WriteLine(data.ToHtmlTable(hash, "sortable-theme-bootstrap").FormatHTML());
                Console.WriteLine("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/sortable/0.8.0/js/sortable.min.js\"></script>");
                Console.WriteLine("<link type=\"text/css\" charset=\"UTF-8\" rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/sortable/0.8.0/css/sortable-theme-bootstrap.min.css\">");
                Console.WriteLine($"<script>table = document.getElementById('{hash}'); table.setAttribute('data-sortable', ''); Sortable.init();</script>");
                Console.WriteLine("</body>");
                Console.WriteLine("</html>");
                return;
            }

            if (format.ToLower() == "table")
            {
                ConsoleTable.PrintToConsole(data.ToStringMatrix());                
                return;
            }

            Console.WriteLine(data.ToFormated(format));
        }
        
        public string ToSerializedXmlString()
        {
            return new XmlLoader<Command>().ToSerialized(this);
        }

        public static Command Load(string commandFileName)
        {
            return new XmlLoader<Command>(Program._config.CommandsLocation).Load(commandFileName);  
        }

        public static List<Command> FindAll()
        {
            return new XmlLoader<Command>(Program._config.CommandsLocation).FindAll();
        }

        public static void Write(Command command)
        {
            new XmlLoader<Command>(Program._config.CommandsLocation).Write(command);
        }

        public static bool Exists(string commandFileName)
        {
            return new XmlLoader<Command>(Program._config.CommandsLocation).Exists(commandFileName);
        }

        public static void Delete(string commandFileName)
        {
            new XmlLoader<Command>(Program._config.CommandsLocation).Delete(commandFileName);
        }

        public static Command GenerateTemplate(string commandName)
        {
            if(Exists(commandName))
                throw new Exception($"File {commandName} already exists!");

            Command c = new Command();
            c.FileName = commandName;
            c.Help = "Help text";

            Step s = new Step();
            s.Connections = new List<string>
            {
                "postgres"
            };

            s.Sql = "SELECT CURRENT_TIMESTAMP";

            s.Format = "table";

            c.Steps = new List<Step>();
            c.Steps.Add(s);

            return c;
        }

        public static void Rename(string oldName, string newName)
        {
            if (Exists(newName))
                throw new Exception($"File {newName} already exists!");

            Command c = Load(oldName);
            c.FileName = newName;
            Write(c);
            Delete(oldName);
        }
    }

    public class Step
    {
        public List<string> Connections;
        public string Sql;
        public string Format;
        
        public string Hash
        {
            get
            {
                return (Format + Sql + String.Join("", Connections)).ToHash();
            }
        }
    }
}
