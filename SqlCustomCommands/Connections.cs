﻿using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlCustomCommands
{
    public class Connections : IDisposable
    {  
        public List<ConnectionWorker> AllConnections {get;set;}      
     
        public Connections(Machine connectionGroup, Command com)
        {
            List<string> usedConnections = new List<string>();
            foreach (Step s in com.Steps)
                usedConnections.AddRange(s.Connections);

            AllConnections = connectionGroup.Connections.Where(con => usedConnections.Contains(con.Name)).Select(c => new ConnectionWorker(c)).ToList();

            OpenConnections();
            BeginTransactions();
        }

        private void OpenConnections()
        {
            Program.WriteVerbose($"Abrindo conexões em {ConnectionsName()}...");
            AllConnections.ForEach(c => c.OpenConnections());            
        }

        public void BeginTransactions()
        {
            Program.WriteVerbose($"Iniciando transações em {ConnectionsName()}...");
        }

        public void Commit()
        {
            Program.WriteVerbose($"Commitando transações em {ConnectionsName()}...");
        }

        public void Rollback()
        {
            Program.WriteVerbose($"Revertendo transações em {ConnectionsName()}...");
        }

        private void CloseConnections()
        {
            Program.WriteVerbose($"Fechando conexões em {ConnectionsName()}...");
            AllConnections.ForEach(c => c.CloseConnections());            
        }

        public string ConnectionsName()
        {
            return string.Join(", ", AllConnections.Select(c => c.ConnectionFile.Name).OrderBy(n => n));
        }

        public void Dispose()
        {
            Rollback();
            CloseConnections();
        }
    }

    public class ConnectionWorker
    {
        public Connection ConnectionFile;
        public NpgsqlConnection Con { get; set; }

        public ConnectionWorker(Connection connectionFile)
        {
            this.ConnectionFile = connectionFile;
        }

        public void OpenConnections()
        {
            Con = new NpgsqlConnection(ConnectionFile.ConnectionString);
            Con.Open();
        }

        public void BeginTransactions()
        {

        }

        public void Commit()
        {

        }

        public void Rollback()
        {

        }

        public void CloseConnections()
        {
            Con.Close();
            Con.Dispose();
        }
    }
}
