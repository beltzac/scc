﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace SqlCustomCommands
{
    public class XmlLoader<T> where T : XmlFile
    {
        public string BasePath { get; set; }

        public XmlLoader() : this(null)
        {
        }

        public XmlLoader(string basePath)
        {
            BasePath = basePath ?? AppDomain.CurrentDomain.BaseDirectory;
        }

        private string InBasePath(string file)
        {
            return Path.Combine(BasePath, file);
        }

        public string ToSerialized(T thing)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true,
                NewLineOnAttributes = true
            };

            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            using (var writer = new StringWriter())
            using (var xmlWriter = XmlWriter.Create(writer, settings))
            {
                serializer.Serialize(xmlWriter, thing, namespaces);
                return writer.ToString();
            }
        }

        public void Write(T thing)
        {
            string fileLocation = InBasePath($"{thing.FileName}.xml");
            Program.WriteVerbose($"Writing {fileLocation}");
            using (var writer = new StreamWriter(fileLocation))
            {
                writer.Write(ToSerialized(thing));
            }
        }

        public T Load(string CommandFileName)
        {
            string fileLocation = InBasePath($"{CommandFileName}.xml");
            Program.WriteVerbose($"Loading {fileLocation}");
            XmlSerializer serializer = new XmlSerializer(typeof(T));            
            using (StreamReader r = new StreamReader(fileLocation, Encoding.Default))
            {               
                T x = (T)serializer.Deserialize(r);
                x.FileName = CommandFileName;
                x.FileLocation = fileLocation;
                return x;
            }
        }

        public bool Exists(string CommandFileName)
        {
            string fileLocation = InBasePath($"{CommandFileName}.xml");
            return File.Exists(fileLocation);
        }

        public void Delete(string CommandFileName)
        {            
            string fileLocation = InBasePath($"{CommandFileName}.xml");
            Program.WriteVerbose($"Deleting {fileLocation}");
            File.Delete(fileLocation);
        }

        public List<T> FindAll()
        {
            string[] filePaths = Directory.GetFiles(BasePath, "*.xml", SearchOption.TopDirectoryOnly);

            List<T> lstObj = new List<T>();
            foreach (string path in filePaths)
            {                               
                try
                {
                    T c = Load(Path.GetFileNameWithoutExtension(path));
                    lstObj.Add(c);
                }
                catch (Exception ex)
                {
                    Program.WriteVerbose($"{path} is not a valid {typeof(T).Name}");
                }
            }
            return lstObj;
        }
    }
}