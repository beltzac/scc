﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SqlCustomCommands
{
    public class Config : XmlFile
    {        
        public const string CONFIG_NAME = "config";

        public string CommandsLocation { get; set; }
        public string CurrentMachine { get; set; }
        public List<Machine> Machines { get; set; }

        private Config()
        {
        }

        public Machine GetCurrentMachine()
        {
            return Machines.First(g => g.Name.ToLower() == CurrentMachine.ToLower());
        }

        public static Config Load()
        {
            return new XmlLoader<Config>().Load(CONFIG_NAME);
        }
        
        public static void  Write(Config config)
        {
            new XmlLoader<Config>().Write(config);
        }

        public static bool Exists()
        {
            return new XmlLoader<Config>().Exists(CONFIG_NAME);
        }

        public static Config GenerateDefault()
        {
            Config c = new Config();
            c.FileName = CONFIG_NAME;
            c.CommandsLocation = Directory.GetCurrentDirectory();
            c.CurrentMachine = "Base1";
            c.Machines = new List<Machine>();
            c.Machines.Add(new Machine
            {
                Name = "Base1",
                Connections = new List<Connection>
                {
                    new Connection
                    {
                        Name = "postgres",
                        ConnectionString = "place_here_the_connection_string"
                    }
                }
            });
            return c;
        }
    }

    public class Machine
    {
        public string Name { get; set; }
        public List<Connection> Connections { get; set; }

    }

    public class Connection
    {
        public string Name { get; set; }
        public string ConnectionString { get; set; }
    }
}
