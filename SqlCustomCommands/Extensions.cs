﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SqlCustomCommands
{
    public static class Extensions
    {
        public static string ToCsv(this DataTable dataTable)
        {
            StringBuilder sbData = new StringBuilder();

            // Only return Null if there is no structure.
            if (dataTable.Columns.Count == 0)
                return null;

            foreach (var col in dataTable.Columns)
            {
                if (col == null)
                    sbData.Append(",");
                else
                    sbData.Append("\"" + col.ToString().Replace("\"", "\"\"") + "\",");
            }

            sbData.Replace(",", System.Environment.NewLine, sbData.Length - 1, 1);

            foreach (DataRow dr in dataTable.Rows)
            {
                foreach (var column in dr.ItemArray)
                {
                    if (column == null)
                        sbData.Append(",");
                    else
                        sbData.Append("\"" + column.ToString().Replace("\"", "\"\"") + "\",");
                }
                sbData.Replace(",", System.Environment.NewLine, sbData.Length - 1, 1);
            }

            return sbData.ToString().Trim();
        }

        public static string ToHtmlTable(this DataTable data, string hash, params string[] classes)
        {
            string[] table = new string[data.Rows.Count];            

            string header = "<thead><tr><th>" + String.Join("</th><th>", (from dc in data.Columns.Cast<DataColumn>() select dc.ColumnName.EncodeHTML()).ToArray()) + "</th></tr></thead>";

            long counter = 0;
            foreach (DataRow row in data.Rows)
            {  
                table[counter] = "<tr><td>" + String.Join("</td><td>", (from o in row.ItemArray select o.ToString().EncodeHTML()).ToArray()) + "</td></tr>";
                counter++;
            }

            return $"<table id=\"{hash}\" class=\"{string.Join(" ", classes)}\"> {header} <tbody> {String.Join("", table)} </tbody> </table>";
        }

        public static string[,] ToStringMatrix(this DataTable data)
        {
            string[,] table = new string[data.Rows.Count + 1, data.Columns.Count];
           
            string[] labels = (from dc in data.Columns.Cast<DataColumn>() select dc.ColumnName).ToArray();

            for (int j = 0; j < data.Columns.Count; j++)
            {
                table[0, j] = labels[j];
            }     

            for (int i = 0; i < data.Rows.Count; i++)
            {
                for (int j = 0; j < data.Columns.Count; j++)
                {
                    table[i + 1,j] = data.Rows[i][j].ToString();
                }
            }

            return table;
        }

        public static string ToFormated(this DataTable data, string format)
        {
            string[] linhasFormatadas = data.Select().Select(r => string.Format(format, r.ItemArray)).ToArray();
            return string.Join(Environment.NewLine, linhasFormatadas);            
        }

        public static string FormatHTML(this string html)
        {
            return System.Xml.Linq.XElement.Parse(html).ToString();
        }

        public static string EncodeHTML(this string text)
        {
            return WebUtility.HtmlEncode(text);
        }

        public static string ToHash(this string text)
        {
            return String.Format("{0:X}", text.GetHashCode());
        }
    }
}
