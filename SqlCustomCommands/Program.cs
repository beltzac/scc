﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SqlCustomCommands
{
    class Program
    {

        public static Dictionary<string, string> MetaCommands;

        public static bool Verbose { get; set; }

        public static bool Timed { get; set; }

        public static Config _config;

        public const string VERSION = "1.0.1";
        public static readonly string BUILD_DATE = Properties.Resources.BuildTimestamp.Trim();

        public static void WriteVerbose(string line)
        {
            if (Verbose)
                WriteColor($"[{DateTime.Now}] {line}", ConsoleColor.Green);
        }

        public static void WriteError(string line)
        {
            WriteColor($"[V{VERSION}] {line}", ConsoleColor.Red);
        }

        public static void WriteColor(string line, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(line);
            Console.ResetColor();
        }

        static void Main(string[] args)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();            
            try
            {
                MetaCommands = new Dictionary<string, string>
                {
                    { "v", "Verbose/Debug mode" },
                    { "u", "Specify the machine config to be used" },
                    { "m", "List existing machines" },
                    { "reformat", "Reformat all command's XMLs" },
               //     { "print", "Print command's programming" },
                    { "xml", "Print command's RAW code" },
                    { "template", "Generate a empty command file" },
                    { "ren", "Rename a command file" },
                    { "t", "Time a command" }
                };

                Verbose = args.Contains("-v");
                Timed = args.Contains("-t");

                List<string> parameters = args.Where(p => !MetaCommands.ContainsKey(p.TrimStart('-'))).ToList();

                WriteVerbose($"Args: {String.Join(" ", args)}");
                WriteVerbose($"Command Parameters: {String.Join(" ", parameters)}");

                if (Config.Exists())
                {
                    _config = Config.Load();
                }
                else
                {
                    _config = Config.GenerateDefault();
                    Config.Write(_config);
                }

                if (args.Contains("-u"))
                {
                    _config.CurrentMachine = parameters.First();
                    Config.Write(_config);
                    return;
                }

                if (args.Contains("-m"))
                {
                    Console.WriteLine($"Current: {_config.CurrentMachine}");
                    Console.WriteLine($"Avaliable: {String.Join(", ", _config.Machines.Select(m => m.Name))}");
                    return;
                }

                if (args.Contains("-reformat"))
                {
                    ReformatCommands();
                    return;
                }

                //if (args.Contains("-print"))
                //{
                //    PrintCommand(parameters);
                //    return;
                //}

                if (args.Contains("-xml"))
                {
                    PrintCommandXml(parameters);
                    return;
                }

                if (args.Contains("-template"))
                {
                    GenerateTemplate(parameters);
                    return;
                }

                if (args.Contains("-ren"))
                {
                    Rename(parameters);
                    return;
                }

                if (parameters.Count() == 0)
                {
                    Help();
                    return;
                }                                                   

                Command c = Command.Load(parameters.First().ToLower());

                using (Connections con = new Connections(_config.GetCurrentMachine(), c))
                {        
                    c.RunCommand(parameters, con);
                    con.Commit();
                }
            }
            catch (Exception ex)
            {                
                WriteError(ex.Message);
                WriteError(ex.StackTrace);
            }
            finally
            {         
                watch.Stop();
                if (Timed)
                    WriteColor($"{watch.ElapsedMilliseconds}ms", ConsoleColor.Yellow);        
            }
        }

        public static void Help()
        {
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine($"SQL Custom Commands {VERSION} (Compiled {BUILD_DATE})");
            Console.WriteLine("Alexandre Beltzac (beltzac@gmail.com)");
            Console.WriteLine("https://gitlab.com/beltzac/scc");
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine();
            List<Command> commands = Command.FindAll();

            foreach (string c in MetaCommands.Keys.OrderBy(c => c))            
                Console.WriteLine($"-{c} -> {MetaCommands[c] ?? "Help not defined"}");
            

            foreach (Command c in commands.OrderBy(c => c.FileName))            
                Console.WriteLine($"{c.FileName} -> {c.Help ?? "Help not defined"}");

            Console.WriteLine();
            Console.WriteLine("--------------------------------------------");
        }

        public static void ReformatCommands()
        {        
            List<Command> commands = Command.FindAll();
            foreach (Command c in commands)              
                Command.Write(c);            
        }

        public static void PrintCommandXml(List<string> parameters)
        {
            Command c = Command.Load(parameters.First());
            Console.WriteLine(c.FileLocation);
            Console.WriteLine(c.ToSerializedXmlString());
        }

        public static void PrintCommand(List<string> parameters)
        {
            Command c = Command.Load(parameters.First());
            Console.WriteLine(c.FileLocation);
            Console.WriteLine(c.ToString());
        }

        public static void GenerateTemplate(List<string> parameters)
        {
            try
            {
                Command c = Command.GenerateTemplate(parameters.First());
                Command.Write(c);
            }
            catch (Exception ex)
            {
                WriteError(ex.Message);
            }
        }

        public static void Rename(List<string> parameters)
        {
            try
            {
                Command.Rename(parameters.First(), parameters.Skip(1).First());
            }
            catch (Exception ex)
            {
                WriteError(ex.Message);
            }
        }
    }
}
